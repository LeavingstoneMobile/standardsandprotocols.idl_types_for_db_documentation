namespace DB {

    export interface Table {
        associatedTypes?:Array<any> // ეს ველი გამოიყენება დოკუმენტაციის მიზნებისათვის ასოცირებული ტიპების type safe ფორმით ჩამოთვლისათვის
    }

    export interface ColumnTypeBase { _66235eebc0514029ab8d8fbe70da2bed:any }

    export interface VarChar extends ColumnTypeBase { }
    export interface GUID extends ColumnTypeBase { }
    export interface Integer extends ColumnTypeBase { }
    export interface Decimal extends ColumnTypeBase { }
    export interface Text extends ColumnTypeBase { }
    export interface DateTime extends ColumnTypeBase { }

    export interface ForeignKey<RemoteTable extends Table,
    FKNullability extends ColumnNullabilityBase> {
        (table: RemoteTable): ColumnBase
    }

    export interface ColumnNullabilityBase{ _116566b338c54e8da4dcca53c57f24ff:any }

    export interface Nullable extends ColumnNullabilityBase{}
    export interface NotNullable extends ColumnNullabilityBase{}

    export interface ColumnAutoIncrementiblity { _d3ffe1f6ebd749578f259ff37642d796:any }

    export interface AutoIncrement extends ColumnAutoIncrementiblity{}
    export interface NoAutoIncrement extends ColumnAutoIncrementiblity{}

    export interface ColumnBase{ _6593d03c985b4a9598e3434aff890880:any }

    export interface Column
    <ColumnType extends ColumnTypeBase,
    Nullability extends ColumnNullabilityBase,
    _AutoIncrement extends ColumnAutoIncrementiblity> 
    extends ColumnBase{}

    export interface Column2
    <ColumnType extends ColumnTypeBase,
    Nullability extends ColumnNullabilityBase,
    _AutoIncrement extends ColumnAutoIncrementiblity,
    ReferencedType> 
    extends ColumnBase{}    

    export interface DefaultColumn<_ColumnType extends ColumnTypeBase>
    extends Column<_ColumnType, NotNullable, NoAutoIncrement> {}

    export interface PrimaryKeyAutoIncrementColumn extends Column<DB.Integer, DB.NotNullable, DB.AutoIncrement>{}

    namespace Examples {

        interface Table1 extends Table 
        {
            id: Column<Integer, NotNullable, AutoIncrement>
        }

        class Table2 implements Table 
        {
            id: DefaultColumn<Integer>
            profileId: ForeignKey<Table1, NotNullable> = (table) => table.id
            userName: DefaultColumn<Text>
            password: DefaultColumn<Text>
        }


    }


}